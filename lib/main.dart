import 'package:flutter/material.dart';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.grey.shade600,
      size: 30.0,
    ),
    actions: <Widget>[
      Icon(
        Icons.more_vert,
        color: Colors.grey.shade600,
        size: 30.0,
      ),
    ],
    title: Text(
      "New York",
      style: TextStyle(
        color: Colors.white,
        fontFamily: 'Neue Helvetica',
        fontWeight: FontWeight.w300,
        fontSize: 35.0,
      ),
    ),
    centerTitle: true,
  );
}

// Widget buildBodyWidget() {
//   return ListView(
//     children: <Widget>[
//       Column(
//         children: <Widget>[
//           Container(
//             width: double.infinity,
//             height: double.infinity,
//             child: Image.network(
//               "https://mcdn.wallpapersafari.com/medium/53/64/CYTh06.jpg",
//               fit: BoxFit.cover,
//             ),
//           )
//         ],
//       )
//     ],
//   );
// }

Widget buildBodyWidget() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/breezy.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          children: <Widget>[
            Container(
              height: 150,
              margin: EdgeInsets.all(20),
              child: ListView(
                children: [
                  Text(
                    "-9°",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Neue Helvetica',
                      fontWeight: FontWeight.w200,
                      fontSize: 70.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Breezy",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Neue Helvetica',
                      fontWeight: FontWeight.w400,
                      fontSize: 20.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'H:-8° L:-14°',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Neue Helvetica',
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Container(
              height: 200,
              child: ListView(
                children: <Widget>[
                  Container(
                    height: 180,
                    margin: EdgeInsets.all(10),
                    decoration: ShapeDecoration(
                      color: Colors.black12,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                    ),
                    child: ListView(
                      children: [
                        Container(
                          margin: EdgeInsets.all(15),
                          child: Text(
                            'Windy conditions will continue for the rest of the day.',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                        Divider(
                          color: Colors.grey.shade500,
                        ),
                        Container(
                          child: weatherList(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 450,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.black12,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              child: ListView(
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 5),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Icon(
                            Icons.sunny,
                            color: Colors.grey,
                            size: 20,
                          ),
                          Text(
                            '  10 - DAY FORECAST',
                            style: TextStyle(
                              color: Colors.grey.shade500,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      )),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 5, bottom: 10),
                      child: weatherForecastList()),
                ],
              ),
            ),
            Container(
              height: 180,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.black12,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              child: ListView(
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 5),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Icon(
                            Icons.grain,
                            color: Colors.grey,
                            size: 20,
                          ),
                          Text(
                            '  AIR QUALTITY',
                            style: TextStyle(
                              color: Colors.grey.shade500,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      )),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Row(
                      children: [
                        Text("42 - Good",style: TextStyle(
                          fontSize: 40.0,
                          fontWeight: FontWeight.w400,color: Colors.white
                        ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(15),
                    child: Text(
                      'Air qualtity index is 42, which is similar to yesterday at about this time.',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 280,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.black12,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              child: ListView(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: [
                        Icon(
                          Icons.umbrella,
                          color: Colors.grey,
                          size: 20,
                        ),
                        Text(
                          '  PRECIPITATION',
                          style: TextStyle(
                            color: Colors.grey.shade500,
                            fontSize: 16.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Image.network("https://www.i-pic.info/i/wfTE328380.jpg",
                      fit: BoxFit.cover,width: 150,height: 220,),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget weatherForecastList() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      forecast1(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast2(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast3(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast4(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast5(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast6(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast7(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast8(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast9(),
      Divider(
        color: Colors.grey.shade500,
      ),
      forecast10(),
    ],
  );
}

// Widget forecast1() {
//   return Column(
//     crossAxisAlignment: CrossAxisAlignment.start,
//     children: <Widget>[
//       Text(
//         "Today",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Sun",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Mon",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Tue",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Wed",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Thu",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Fri",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Sat",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Sun",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//       Divider(),
//       Text(
//         "Mon",
//         style: TextStyle(
//             fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
//       ),
//     ],
//   );
// }
//
// Widget forecast2() {
//   return Column(
//     crossAxisAlignment: CrossAxisAlignment.start,
//     children: <Widget>[
//       Icon(
//         Icons.wind_power,
//         color: Colors.white,
//       ),
//       Divider(),
//       Icon(
//         Icons.sunny,
//         color: Colors.orangeAccent,
//       ),
//       Divider(),
//       Icon(
//         Icons.cloud,
//         color: Colors.white,
//       ),
//       Divider(),
//       Icon(
//         Icons.sunny,
//         color: Colors.orangeAccent,
//       ),
//       Divider(),
//       Icon(
//         Icons.sunny,
//         color: Colors.orangeAccent,
//       ),
//       Divider(),
//       Icon(
//         Icons.sunny,
//         color: Colors.orangeAccent,
//       ),
//       Divider(),
//       Icon(
//         Icons.sunny,
//         color: Colors.orangeAccent,
//       ),
//       Divider(),
//       Icon(
//         Icons.thunderstorm,
//         color: Colors.white,
//       ),
//       Divider(),
//       Icon(
//         Icons.thunderstorm,
//         color: Colors.white,
//       ),
//       Divider(),
//       Icon(
//         Icons.sunny,
//         color: Colors.orangeAccent,
//       ),
//     ],
//   );
// }
//
// Widget forecast3() {
//   return Column(
//       crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[]);
// }
//
// Widget forecast4() {
//   return Column(
//       crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[]);
// }

Widget forecast1() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Today",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.air,
        color: Colors.white,
      ),
      Text(
        "-14°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "8°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}

Widget forecast2() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Sun",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-9°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "-3°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast3() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Mon",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
      ),
      Text(
        "-8°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "-2°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast4() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Tue",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-4°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "2°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast5() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Wed",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-5°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "3°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast6() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Thu",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-2°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "7°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast7() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Fri",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "1°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "10°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast8() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Sat",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.thunderstorm,
        color: Colors.white,
      ),
      Text(
        "3°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "9°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast9() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Sun",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.thunderstorm,
        color: Colors.white,
      ),
      Text(
        "9°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "15°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget forecast10() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text(
        "Mon",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "5°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Text(
        "12°",
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
      )
    ],
  );
}

Widget weatherList() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildWeather1(),
        buildWeather2(),
        buildWeather3(),
        buildWeather4(),
        buildWeather5(),
        buildWeather6(),
      ]);
}

Widget buildWeather1() {
  return Column(
    children: <Widget>[
      Text(
        "Now",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-9°",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}

Widget buildWeather2() {
  return Column(
    children: <Widget>[
      Text(
        "08",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-8°",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}

Widget buildWeather3() {
  return Column(
    children: <Widget>[
      Text(
        "09",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.cloudy_snowing,
        color: Colors.white,
      ),
      Text(
        "-7°",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}

Widget buildWeather4() {
  return Column(
    children: <Widget>[
      Text(
        "10",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.cloudy_snowing,
        color: Colors.white,
      ),
      Text(
        "-6°",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}

Widget buildWeather5() {
  return Column(
    children: <Widget>[
      Text(
        "11",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.air,
        color: Colors.white,
      ),
      Text(
        "-5°",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}

Widget buildWeather6() {
  return Column(
    children: <Widget>[
      Text(
        "12",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "-5°",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.white),
      ),
    ],
  );
}
